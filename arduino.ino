// include the library code:
#include <LiquidCrystal.h>
#include <Servo.h>

// initialize the library by associating any needed LCD interface pin
// with the arduino pin number it is connected to
const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
const int pinOogLinks = A0;
const int pinOogRechts = A1;
const int servoPin = 6;

LiquidCrystal lcd(rs, en, d4, d5, d6, d7);
Servo blauwtje;

void setup() {
    // set up the LCD's number of columns and rows:
    lcd.begin(16, 2);
    // Print a message to the LCD.
    lcd.print("Hallo Jessie!");
    blauwtje.attach(servoPin);
}

int draaien = MIDDEN;
bool draaiNaarRechts = true;

void herhaal() {
    int oogLinks = leesOogLinks();
    int oogRechts = leesOogRechts();

    if (oogRechts < oogLinks) {
        if (draaiNaarRechts) {
            draaien += 1;
        } else {
            draaien -= 1;
        }
    }

    if (draaien > RECHTS) {
        draaiNaarRechts = false;
    }
    if (draaien < 0) {
        draaiNaarRechts = true;
    }

    draai(draaien); 
    wacht(); 
}