Simple Robot
============

My daughter (7 years old) had to give a presentation at school, just to learn to talk to a group. Her sister did a presentation once where she created a movie using Scratch, so she, too, wanted to do something with programming. I didn't want her to give the very same presentation that her sister did, though.

Around this time there happened to be a Robot Do-Expo in town. It was a big disappointment where they ended up having to draw a robot and then making it from clay. It did give an idea for the presentation though: not so long ago I bought an Arduino kit. She was thrilled at the idea of making her own robot.

So here it is: a robot that can see and that will turn its head if you put your hand in front of it. It turned out great: not only could she do some programming (with a lot of help), but there was also a lot of creativity involved.

Setup
-----

Below I give a very short summary of how things work. It should be enough information so that you know what to Google for.

### Arduino IDE

Before you can do anything at all one must download and install the Arduino IDE. This is the environment in which I created the first Hello World project (that is: blinking the on board LED).

It turned out not to be too hard. The IDE contains some examples and after connecting the Arduino to the laptop using the USB cable, all I had to do was select the right board and port, compile and upload.

The Arduino IDE has a horrible UI, though.

### Visual Studio Code

I did most of my programming in Visual Studio Code. It took some effort to get it working and it required some Googling, but at the end it was very convenient working in VSCode.

All it requires is that you install the Arduino Extention. (In `Extensions` just search for "Arduino"). You have to enter the path to the Arduino IDE somewhere and, just like in the Arduino IDE, you have to select the board and port.

After that the most confusing part was that I tried to get debugging working, but it's simply not supported by my board. After coming to that conclusion I simply used the `Arduino: Upload` command and that was it.

### Servo motor

The kit came with a Servo Motor. It has three wires: one for power, one for 0 and one for the desired position. Just connect the wire for the desired position to one of the Pulse Width Modulation pins on the Arduino board and then you can turn the servo motor to the most left position by writing "0" to that pin, or to the most right position by writing "180" to that pin. (It doesn't go higher than that if I understood correctly).

### Buttons

I don't remember exactly how I connected buttons. Either one has

* power, button, resistor, 0 or
* power, resistor, button, 0.

In both cases a wire goes from between the button and resistor to the Arduino board. I think I used the first one, as it would give a HIGH if the button is pressed, which makes sense.

### Eye

Same here: I'm not sure how I connected it. Either it was:

* power, LDR, resistor, 0 or
* power, resistor, LDR, 0

In both cases a wire has to go from between the LDR and resistor to one of the Analog Inputs of the board.

A Light Dependent Resistor (LDR) has a lower resistance if there is more light. So I think I used the first configuration as it would mean that more light would send a higher voltage to the board.

Internet sites are not consistent in what resistor should be used. The resistor you need strongly depends on the LDR you have. In my case I got a reasonable spectrum between light and dark using a 10k resistor.

When reading the analog input I expected a value between 0 and 255. It was between 0 and 1023.

### LCD

The LCD is extremely convenient for debugging, as there is basically no other way to get information out of the device, other than using an LED to signal that something is either true or false. So I decided to figure out how to use the LCD that came with the kit. It was pretty easy!

I found the following website: https://www.arduino.cc/en/Tutorial/LiquidCrystalDisplay. I connected the wires as indicated (had to change some pins for the servo motor because they were now used by the LCD), used the sample code and it worked in one go.

Development approach
--------------------

I had no experience with programming the Arduino. So it didn't sound smart to immediately start programming against LDRs and servo motors. So we took it gradually.

The first attempt was to light the onboard LED. That wasn't too hard.

The next attempt was to control the servo motor. So we connected it to the bread board, connected the bread board to the Arduino board and programmed the servo motor to go to its farmost left position, wait a while, go to the center prosition, wait a while, go to the farmost right position, wait a while, repeat. Now we now how to control the servo motor.

The next step was being able to respond to input. So we put a button on the bread board with a resistor, connected it to one of the Arduino's I/O ports and then programmed the Arduino to put the servo motor in its left position if the button was pressed and its right position if the button was not pressed.

The next attempt was trying to respond to the LDR. We tried different resistors in combination with the LDR, connected the LDR to one of the analog inputs and programmed the Arduino to read the analog input and use that as the position for the servo motor (capped to 180). I didn't know the highest input value is 1023 at the time. So we put solid straws over the LDR's so it would only get a little light from the environment. Now the servo would change its position depending on how close you put your hands to the eyes.

The last attempt I did by myself, because investigating why I didn't work as expected was to advanced for a 7-year old. Only after connecting the LCD and displaying the value from the analog inputs I found out that, indeed, they go to 1023 instead of to 255... After that it was quite easy to come up with an "algorithm" to decide when to turn the head.

Finishing up
------------

The above should give enough information to get going and together with the source code end up with a servo motor that rotates if you block an eye. All that's left is to turn it into an actual robot. This was actually my daughter's favorite part of this project :-).

For the body we used a small cardboard box that I happened to receive when ordering some stuff online. At the position of the neck we carved out a square space that was slightly smaller than the base of the servo motor, so that when you push the servo motor into that hall it's more or less fixed.

The kit also came with small pieces of plastic that you can push onto the axis of the servo motor. For the head we used a piece of cardboard the size of approximately an A4 paper, drew 6 squares on it of approximately 6 x 6 cm in the shape of a cross and folded it into a cube. We sew the piece of plastic onto the center piece of the 6 squares. The eyes were attached to the top of the cross. So when you fold the cube it opens on the top.

I mentioned "eyes". Yes, the face contained two LDRs. But only one was connected to the arduino. The arduino board also contained an LDR that was connected to one of the pins. The idea is that if the amount of light on the head is less than the amount of light on the belly, then the face is blocked and the robot should rotate its head.

The last finishing touch is my daughter decorating the robot so it looks kinda nice.
